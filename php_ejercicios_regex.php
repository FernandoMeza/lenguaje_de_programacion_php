<?php
//Nombre: Meza Ortega Fernando
//Realizar una expresión regular que detecte emails correctos.

//En realidad no creo que detecte todos los posibles email porque eso suena
//imposible con una expresión regular, ya que según el RFC5321 parece que existen
//muchas reglas que serían muy complicadas de verificar 
$email = "/[a-zA-Z0-9_\-.]+@[a-zA-Z]+\.[a-zA-Z]+/";

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.

//Según lo que investigué el CURP es mucho más complejo que esto, de hecho ni
//siquiera detecta mi CURP con los caracteres del comentario
$CURP = "/[ABCD123456EFGHIJ78]{18}/";
//En el siguiente comentario se muestra una mejor opción (aunque tampoco tan
//estricto como debería:
//$CURP = "/[A-Z0-9]{18}/";

//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.

$letras = "/[a-zA-Z]{50}[a-zA-Z]*/"; 

//Crea una funcion para escapar los simbolos especiales.

//??

//Crear una expresion regular para detectar números decimales.

$decimales = "/([-\+])?([0-9]+(\.[0-9]+)?)/";

?>