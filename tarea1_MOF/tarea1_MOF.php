<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			div 
			{
		    	display: flex;
    			justify-content: center;
    			align-items: center;
    		}
		</style>
		<title>Tarea 1 MOF</title> 
	</head> 
	<body> 
		<?php 
		//Nombre: Meza Ortega Fernando
		//Se colocan el número de filas que pide el ejercicio
		$Filas = 30;
		//Primero vamos a mostrar las imagenes de la piramide
		print "Pirámide: ";
		for ($i = 1; $i <= $Filas; ++$i)
		{
			echo "<div>";
			for ($j = 1; $j <= $i; ++$j)
			{
				echo "<img src='Golden_star.png' style='width:1.5%'>";
			}
			echo "</div>";
		}
		print "Rombo: ";
		for ($i = 1; $i <= $Filas; ++$i)
		{
			echo "<div>";
			for ($j = 1; $j <= $i; ++$j)
			{
				echo "<img src='Golden_star.png' style='width:1.5%'>";
			}
			echo "</div>";
		}

		for ($i = $Filas - 1; $i >= 1; --$i)
		{
			echo "<div>";
			for ($j = 1; $j <= $i; ++$j)
			{
				echo "<img src='Golden_star.png' style='width:1.5%'>";
			}
			echo "</div>";
		}
		?>
	</body>
</html> 