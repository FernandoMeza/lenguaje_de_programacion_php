<?php
//Etiqueta para activar la sesión
session_start();
?>

<html lang="es">
    <head>
        <title> Login </title>
        <meta charset="UFT-8">
        <meta name="viewport" content="width-device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">

        <style>
        .container {
            
            width: 30%;
            height: 30%;
            display: flex;
            justify-content: center;
        }
        </style>

    </head>
    
    <?php
    $_SESSION['Alumno'] = [
        1 => [
            'num_cta' => '1',
            'nombre' => 'Admin',
            'primer_apellido' => 'General',
            'segundo_apellido' => '',
            'contrasena' => 'adminpass123',
            'genero' => 'O',
            'fecha_nac' => '25/01/1998'
        ]
    ];
    ?>


    <body class="container">
        <div>   <?php
        /*
            if (isset($_POST)){
                foreach($_SESSION['Alumno'] as $alumno) {
                    if($alumno['num_cta'])
                }
            }
            */
                echo '<form action="login.php" method="POST">';
                ?>
                    <h1>Formulario</h1>
                    <label class="form-label" for="input-cuenta">Número de cuenta:</label>
                    <input name="num_cta" class="form-input" type="Number" id="input-cuenta" placeholder="Número de Cuenta">
                    <label class="form-label" for="input-contraseña">Contraseña: </label>
                    <input name="contraseña" class="form-input" type="password" id="input-contraseña" placeholder="Contraseña">

                    <!-- Botón:  -->
                    <input type='submit' class="btn btn-primary" value="Entrar"/>
                </form>
        </div>
    </body>
</html>