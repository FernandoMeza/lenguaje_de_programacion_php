<html lang="es">
    <head>
        <title> Info </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width-device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">

        <style>
            .navbar{
                background-color: lightblue;
            }

            #Usuario{
                display: flex;
                width: 60%;
                justify-content: center;
            }

            #Datos {
                display: flex;
                width: 70%;
                justify-content: center;
            }

            div {
                margin-top: 15px;
                margin-bottom:  15px;
            }
        </style>

    </head>
    <body>
    
    <header class="navbar">
        <section class="navbar-section">
            <a href="info.php" class="navbar-brand mr-2">Home</a>
            <a href="formulario.php" class="btn btn-link">Registro Alumnos</a>
        </section>
        <section class="navgar-section">
            <a href="login.php" class="btn btn-link">Cerrar Sesión</a>
        </section>
    </header>

    <h2>Usuario autenticado</h2>
    <div class="container" id="Usuario">
        <table class="table">
            <thead>
                <tr>
                    <th>Meza Ortega Fernando</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Número de cuenta 2</td>
                </tr>
                <tr>
                    <td>Fecha de nacimiento:</td> 
                </tr>            
            </tbody>
        </table>
    </div>
    <h2>Datos guardados</h2>
    <div class="container"  id="Datos">
        <table class="table table-striped">
            <thead>
                <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Fecha de nacimiento</th>
                </tr>
            </thead>
            <tbody>
                <tr class="active">
                <td>The Shawshank Redemption</td>
                <td>Crime, Drama</td>
                <td>14 October 1994</td>
                </tr>
            </tbody>
        </table>
    </div>

    </body>
</html>