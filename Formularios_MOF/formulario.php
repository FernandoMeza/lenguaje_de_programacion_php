<html lang="es">
    <head>
        <title> Formulario </title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width-device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
        <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">

        <style>
            .navbar{
                background-color: lightblue;
            }

            div {
                margin-top: 15px;
                margin-bottom:  15px;
            }

            .container
            {
                width: 80%;
            }
        </style>

    </head>
    <body>
    
    <header class="navbar">
        <section class="navbar-section">
            <a href="info.php" class="navbar-brand mr-2">Home</a>
            <a href="formulario.php" class="btn btn-link">Registro Alumnos</a>
        </section>
        <section class="navgar-section">
            <a href="login.php" class="btn btn-link">Cerrar Sesión</a>
        </section>
    </header>

    <div class="container">
        <form action="info.php" method="POST">
        <!-- Numero de cuenta-->
        <label class="form-label" for="input-cuenta">Número de cuenta:</label>
        <input name="num_cta" class="form-input" type="Number" id="input-cuenta" placeholder="Número de Cuenta">
        <!-- Nombre-->
        <label class="form-label" for="input-text">Nombre</label>
		<input name="nombre" class="form-input " type="text" id="input-nombre" placeholder="Nombre">
        
        <label class="form-label" for="input-text">Primer Apellido</label>
		<input name="primer_apellido" class="form-input " type="text" id="input-pApellido" placeholder="Primer Apellido">

        <label class="form-label" for="input-text">Segundo Apellido</label>
		<input name="segundo_apellido" class="form-input " type="text" id="input-sApellido" placeholder="Segundo Apellido">
        <!-- Género-->
        <label class="form-label">Género</label>
		<label class="form-radio">
		    <input type="radio" name="genero" value="H" checked>
			<i class="form-icon"></i> Hombre
		</label>
		<label class="form-radio">
			<input type="radio" name="genero" value="M">
			<i class="form-icon"></i> Mujer
		</label>
        <label class="form-radio">
			<input type="radio" name="genero" value="O">
			<i class="form-icon"></i> Otro
		</label>

		<!-- form date control -->
		<label class="form-label" for="input-fec_nac">Fecha</label>
			<input name="fec_nac" class="form-input " type="date" id="input-fec_nac" placeholder="Fecha de nacimiento">        

        <!-- Contraseña-->
        <label class="form-label" for="input-contraseña">Contraseña: </label>
        <input name="contrasena" class="form-input" type="password" id="input-contraseña" placeholder="Contraseña">

        <!-- Botón:  -->
    </div>
    <div class="container">
        <input type='submit' class="btn btn-primary" value="Registrar "/>
    </div>

    </body>
</html>